public class Main {

    public static boolean isPairSumSimple(int[] arr, int n, int sum) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] + arr[j] == sum)
                    return true;
            }
        }
        return false;
    }

    public static boolean isPairSum(int[] arr, int n, int sum) {
        int l = 0, r = n - 1;
        while (l < r) {
            if (arr[l] + arr[r] == sum)
                return true;
            else if (arr[l] + arr[sum] > sum)
                r--;
            else l++;
        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1, 4, 7, 4, 7, 3, 7, 3, 8, 3, 2, 5, 7};
        System.out.println(isPairSum(arr, arr.length, 10));
        System.out.println(isPairSumSimple(arr, arr.length, 10));
    }
}
